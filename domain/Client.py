from errors.ClientException import ClientError

__author__ = 'Vlad'

class Client(object):
    def __init__(self,id, name, cnp):
        self._setId(id)
        self.setName(name)
        self.setCnp(cnp)

    def _setId(self, id):
        if id == None:
            raise ClientError("Bad id")
        self._id = id

    def setName(self, name):
        self._name = name

    def setCnp(self, cnp):
        self._cnp = cnp

    def getId(self):
        return self._id

    def getName(self):
        return self._name

    def getCnp(self):
        return self._cnp

    def __str__(self):
        return "Client id " + str(self._id) + " name " + str(self._name) + " CNP " + str(self._cnp)

    def __repr__(self):
        return self.__str__()