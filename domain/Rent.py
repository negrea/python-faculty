from datetime import datetime, timedelta

__author__ = 'Vlad'


class Rent:
    def __init__(self, id, client, movie, rentedDays):
        self._id = id
        self._client = client
        self._movie = movie
        self._startDate = datetime.now()
        self._endDate = self._startDate + timedelta(days=rentedDays)
        self._rentedDays = rentedDays
        self._returnDate = None

    def getId(self):
        return self._id

    def getClient(self):
        return self._client

    def getMovie(self):
        return self._movie

    def getStartDate(self):
        return self._startDate

    def getEndDate(self):
        return self._endDate

    def getReturnDate(self):
        return self._returnDate

    def getDaysRented(self):
        return self._rentedDays

    def __str__(self):
        return str(self._client) + "\n" + str(self._movie) + "\n start date " + str(
            self._startDate) + "\n end date " + str(self._endDate)
    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return (self._endDate - self._startDate).days