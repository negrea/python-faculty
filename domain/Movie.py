from errors.MovieException import MovieError

__author__ = 'Vlad'


class Movie:
    def __init__(self, id, title, description, type):
        self._setId(id)
        self.setTitle(title)
        self.setDescription(description)
        self.setType(type)

    def _setId(self, id):
        if id == None:
            raise MovieError("Bad id")
        self._id = id

    def setTitle(self, title):
        self._title = title

    def setDescription(self, description):
        self._description = description

    def setType(self, type):
        self._type = type

    def getTitle(self):
        return self._title

    def getDescription(self):
        return self._description

    def getType(self):
        return self._type

    def getId(self):
        return self._id

    def __str__(self):
        return "Movie id " + str(self._id) + " title " + str(self._title) + " type " + str(self._type) + " desctiption: " + str(self._description)

    def __repr__(self):
        return "Movie id " + str(self._id) + " title " + str(self._title) + " type " + str(self._type) + " desctiption: " + str(self._description)
