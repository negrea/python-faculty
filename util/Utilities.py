import copy

__author__ = 'Vlad'


def gcd(a, b):
    """
    Input: a The first element
           b The second element
    Output: The gcd of a and b

    Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b (so that when
    b is divided by it, the result comes out positive).
    """

    while b:
        a, b = b, a % b
    return a


def TestPrime():
    assert is_prime(0) == False
    assert is_prime(1) == False
    assert is_prime(5) == True
    assert is_prime(2) == True
    assert is_prime(12) == False


'''
Input: The number on which to check if it is prime
    precond: If the number is less than 2, return False
Output: True if the number is prime, False otherwise
'''


def is_prime(no):
    if no < 2:
        return False
    else:
        for i in range(2, no // 2 + 1):
            if no % i == 0:
                return False
    return True


TestPrime()


def TestOdd():
    assert is_odd(0) == False
    assert is_odd(1) == True
    assert is_odd(12) == False


'''
Input: The number on which to check if it is odd
Output: True if the number is odd, False otherwise
'''


def is_odd(no):
    if no % 2 == 0:
        return False
    else:
        return True


TestOdd()

def update_undo(list, undo_list):
    undo_list = copy.deepcopy(list)
    return undo_list


def undo(list, undo_list):
    list = copy.deepcopy(undo_list)
    return list

def print_list(list):
    print("List is " + list)
