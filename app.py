from controllers.ClientsContoller import ClientsContoller
from controllers.MoviesController import MoviesController
from controllers.RentController import RentController
from repository.ClientsRepository import ClientsRepository
from repository.MoviesRepository import MoviesRepository
from repository.RentRepository import RentRepository
from ui.Console import Console

__author__ = 'Vlad'

def start():
    clientsRepo = ClientsRepository()
    moviesRepo = MoviesRepository()
    rentRepo = RentRepository()
    clientsController = ClientsContoller(clientsRepo)
    moviesController = MoviesController(moviesRepo)
    rentController = RentController(rentRepo, moviesRepo, clientsRepo)

    ui = Console(clientsController, moviesController, rentController)

    ui.main_menu()

start()