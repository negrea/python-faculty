from domain.Client import Client
from domain.Movie import Movie
from domain.Rent import Rent
from errors import MovieException
from errors.RentException import RentException

__author__ = 'Vlad'


class Console:
    _nextRentId = 0

    def __init__(self, clientsController, moviesController, rentController):
        self._clientsController = clientsController
        self._moviesController = moviesController
        self._rentController = rentController
        self.populateRents()

    def _printMenu(self):
        str = '\nAvailable commands:\n'
        str += '\t 1 - Print movies\n'
        str += '\t 2 - Print clients\n'
        str += '\t 3 - Print rents\n'
        str += '\t 4 - Add movie\n'
        str += '\t 5 - Remove movie\n'
        str += '\t 6 - Find movie by title\n'
        str += '\t 7 - Update movie\n'
        str += '\t 8 - Add a client\n'
        str += '\t 9 - Remove a client\n'
        str += '\t 10 - Find client by name\n'
        str += '\t 11 - Update client\n'
        str += '\t 12 - Add rent\n'
        str += '\t 13 - Return movie\n'
        str += '\t 14 - Print rents\n'
        str += '\t 15 - Print most rented movies\n'
        str += '\t 16 - Print movies rented by client\n'
        str += '\t 0 - Exit\n'
        print(str)

    def _addRent(self):
        movieTitle = input("Movie title = ")
        clientName = input("Client name = ")

        movies = self._moviesController.findMovieByTitle(movieTitle)
        if not movies:
            print("No movie found ")
            return

        clients = self._clientsController.findClientByName(clientName)
        if not clients:
            print("No clients found ")
            return

        daysRented = self._readPositiveInteger("Days rented = ")

        rent = Rent(self._nextRentId, clients[0], movies[0], daysRented)
        self._rentController.add(rent)
        print("Created rent " + str(rent))

        self._printRentals()

    def _returnRent(self):
        movieTitle = input("Movie title = ")
        clientName = input("Client name = ")

        rent = []
        try:
            rent = self._rentController.findRental(clientName, movieTitle)
        except RentException as e:
            print(e)

        if rent is not None:
            print("Found rentals matching " + str(rent))
        else:
            print("No rentals found matching")

        if rent:
            self._rentController.remove(rent[0])
            print("Rent " + str(rent[0]) + "\nhas been removed")
        else:
            print("Rent not found")

    def _addMovieMenu(self):
        id = self._readPositiveInteger("Movie id =")
        title = input("Title =")
        description = input("Desctiption =")
        type = input("Type =")

        movie = Movie(id, title, description, type)
        self._moviesController.addMovie(movie)

        print("Added new movie: ", movie)

    def _updateMovie(self):
        old_id = self._readPositiveInteger("Old movie id = ")

        if not self._moviesController.isIdUsed(old_id):
            print("Id is not in the list")
            return

        print("Input new movie info:")
        title = input("Title =")
        description = input("Desctiption =")
        type = input("Type =")

        movie = Movie(id, title, description, type)
        self._moviesController.updateMovie(old_id, movie)

    def _updateClient(self):
        old_id = self._readPositiveInteger("Old movie id = ")

        if not self._clientsController.isIdUsed(old_id):
            print("Id is not in the list")
            return

        print("Input new client info:")
        name = input("Name =")
        cnp = self._readPositiveInteger("Cnp =")

        client = Client(id, name, cnp)
        self._clientsController.updateClient(old_id, client)

    def _addClientMenu(self):
        id = self._readPositiveInteger("Client id =")
        name = input("Name =")
        Cnp = input("CNP =")

        movie = Client(id, name, Cnp)
        self._clientsController.addClient(movie)

        print("Added new client: ", movie)

    def _removeMovieMenu(self):
        self._printMovieList(self._moviesController.getAll())
        id = self._readPositiveInteger("Movie id = ")
        self._moviesController.removeMovie(id)

    def _removeClientMenu(self):
        self._printClientList(self._clientsController.getAll())
        name = input("Client name = ")
        self._clientsController.removeClientByName(name)

    def _findMovieMenu(self):
        name = input("Movie name =")
        print("Found movie(s):")
        print(self._moviesController.findMovieByTitle(name))

    def _findClientMenu(self):
        name = input("Client name =")
        print("Found client(s):")
        print(self._clientsController.findClientByName(name))

    def _printMovieList(self, list):
        print("Movies:")
        for m in list:
            print(m)

    def _printClientList(self, list):
        print("Clients:")
        for m in list:
            print(m)

    def _printMovies(self):
        print(self._moviesController.get_all())

    def _printClients(self):
        print(self._clientsController.get_all())

    def _printRentals(self):
        print(self._rentController.get_all())

    def _printMostRentedMovies(self):
        print(self._rentController.getMostRentedMovies())

    def _getMoviesRentedByClient(self):
        searchForClient = self._clientsController.findClientByName(input("Client name"))

        if searchForClient[0] is not None:
            print(self._rentController.getMoviesRentedByClient(searchForClient[0]))
        else:
            print("No client has the given name")

    def main_menu(self):
        keepAlive = True
        while keepAlive:
            self._printMenu()
            command = input("Enter command: ").strip()
            try:
                if command == '0':
                    print("exit...")
                    keepAlive = False
                elif command == '1':
                    self._printMovies()
                elif command == '2':
                    self._printClients()
                elif command == '3':
                    self._printRentals()
                elif command == '4':
                    self._addMovieMenu()
                elif command == '5':
                    self._removeMovieMenu()
                elif command == '6':
                    self._findMovieMenu()
                elif command == '7':
                    self._addClientMenu()
                elif command == '8':
                    self._removeClientMenu()
                elif command == '9':
                    self._findClientMenu()
                elif command == '10':
                    self._updateMovie()
                elif command == '11':
                    self._updateClient()
                elif command == '12':
                    self._addRent()
                elif command == '13':
                    self._returnRent()
                elif command == '14':
                    self._printRentals()
                elif command == '15':
                    self._printMostRentedMovies()
                elif command == '16':
                    self._getMoviesRentedByClient()
                else:
                    print("Invalid command!")
            except MovieException as e:
                print(e)

    @staticmethod
    def _readPositiveInteger(msg):
        """
        Reads a positive integer
        Input: -
        Output: A positive integer
        """
        result = None
        while result == None:
            try:
                result = int(input(msg))
                if result < 0:
                    raise ValueError
            except ValueError:
                print("Please input a positive integer!")
        return result

    def populateRents(self):
        clients = self._clientsController.get_all()
        movies = self._moviesController.get_all()

        rent = Rent(10, clients[0], movies[0], 12)
        self._rentController.add(rent)

        rent = Rent(10, clients[1], movies[0], 12)
        self._rentController.add(rent)

        rent = Rent(10, clients[0], movies[1], 12)
        self._rentController.add(rent)
