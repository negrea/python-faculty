from domain.Movie import Movie
from errors.MovieException import MovieError
from repository.Repo import Repo

__author__ = 'Vlad'

class MoviesRepository(Repo):

    def __init__(self):
        Repo.__init__(self)
        self.prepopulate()

    def add(self, movie):
        """
        Add a movie to the repository
        Input: movie - movie to be added
        Raises movieException in case of duplicate id
        """
        if self._find(movie.getId()) != -1:
            raise MovieError("movie with id - " + str(id) + " already exists")

        Repo.add(self, movie)

    def _find(self, id):
        """
        Return the index of the movie having the given id
        Input: id - the id of the movie we search for
        Output: Index of the movie in the repository's list if found, -1 otherwise
        """
        for i in range(0, len(self)):
            if self.get_item_at_index(i).getId() == id:
                return i
        return -1

    def findById(self, id):
        """
        Return the movie having the given id
        Input: id - The id that we search for
        Output: The movie if found, None otherwise
        """
        index = self._find(id)
        if index == -1:
            return None
        return self.get_item_at_index(index)

    def remove(self, id):
        """
        Remove the movie having the given id
        Input: index - A natural number between 0 and the repo size
        Output: The movie that was removed
        movieException is raised if a movie having the given id does not exist
        """
        index = self._find(id)
        if index == -1:
            raise MovieError("Movie having id - " + str(id) + " is not in repository")
        return self.remove_item_at_position(index)

    def updateMovie(self, id, new_movie):
        movieToUpdate = self.findById(id)

        if movieToUpdate is None:
            raise MovieError("Movie having id - " + str(id) + " is not in repository")

        if new_movie.getTitle() != "":
            movieToUpdate.setTitle(new_movie.getTitle())
        if new_movie.getDescription() != "":
            movieToUpdate.setDescription(new_movie.getDescription())
        if new_movie.getType() != "":
            movieToUpdate.setType(new_movie.getType())

    def prepopulate(self):
        movie = Movie(1, 'a', 'b', 'c')
        self.add(movie)
        movie = Movie(2, 'Name', 'Description231', 'Type213')
        self.add(movie)

    def findMovieByTitle(self, title):
        """
        Find all movies having the given name
        Input: name - the name of the movie being searched for
        Output: List of movies having the given name.
        """
        result = []
        for currentMovie in self.get_all():
            if title == currentMovie.getTitle():
                result.append(currentMovie)
        return result
