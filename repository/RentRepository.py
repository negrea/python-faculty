from domain.Rent import Rent
from errors.RentException import RentException
from repository.Repo import Repo

__author__ = 'Vlad'

class RentRepository(Repo):

    def __init__(self):
        Repo.__init__(self)

    def add(self, rent):
        """
        Add a rent to the repository
        Input: rent - rent to be added
        Raises rentException in case of duplicate id
        """
        Repo.add(self, rent)

    def _find(self, id):
        """
        Return the index of the rent having the given id
        Input: id - the id of the rent we search for
        Output: Index of the rent in the repository's list if found, -1 otherwise
        """
        for i in range(0, len(self)):
            if self.get_item_at_index(i).getId() == id:
                return i
        return -1

    def findByName(self, clientName):
        """
        Return the rental object beeing rented by the client with the given name
        Input: name - Name of the client
        Output: The rental objects in a list
        """
        _foundRentals = []
        _currentRental = None
        for i in range(0, len(self)):
            _currentRental = self.get_item_at_index(i)
            if _currentRental.getClient().getName() == clientName:
                _foundRentals.append(_currentRental)
        return _foundRentals

    def findByNameAndMovie(self, clientName, movieTitle):
        _searchResults = self.findByName(clientName)

        if _searchResults is None :
            raise RentException(clientName + " has no active rentals")

        # Search the results for movie title
        _searchResults = self._searchForMovieTitle(_searchResults, movieTitle)

        if _searchResults is None :
            raise RentException(clientName + " has no active rentals with the title " + movieTitle)

        return _searchResults

    def _searchForMovieTitle(self, rentalsList, movieTitle):
        _foundResults = []
        for rental in rentalsList:
            if rental.getMovie().getTitle() == movieTitle:
                _foundResults.append(rental)

        return _foundResults

    def remove(self, id):
        """
        Remove the rent having the given id
        Input: index - A natural number between 0 and the repo size
        Output: The rent that was removed
        rentException is raised if a rent having the given id does not exist
        """
        index = self._find(id)
        if index == -1:
            raise RentException("Rent having id - " + str(id) + " is not in repository")
        return self.remove_item_at_position(index)