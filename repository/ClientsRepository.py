from domain.Client import Client
from errors.ClientException import ClientError
from repository.Repo import Repo

__author__ = 'Vlad'


class ClientsRepository(Repo):

    def __init__(self):
        Repo.__init__(self)
        self.prepopulate()

    def add(self, client):
        """
        Add a client to the repository
        Input: client - client to be added
        Raises clientException in case of duplicate id
        """
        if self._find(client.getId()) != -1:
            raise ClientError("client with id - " + str(id) + " already exists")

        Repo.add(self, client)

    def _find(self, id):
        """
        Return the index of the client having the given id
        Input: id - the id of the client we search for
        Output: Index of the client in the repository's list if found, -1 otherwise
        """
        for i in range(0, len(self)):
            if self.get_item_at_index(i).getId() == id:
                return i
        return -1

    def findById(self, id):
        """
        Return the client having the given id
        Input: id - The id that we search for
        Output: The client if found, None otherwise
        """
        index = self._find(id)
        if index == -1:
            return None
        return self.get_item_at_index(index)

    def remove(self, id):
        """
        Remove the client having the given id
        Input: index - A natural number between 0 and the repo size
        Output: The client that was removed
        clientException is raised if a client having the given id does not exist
        """
        index = self._find(id)
        if index == -1:
            raise ClientError("Client having id - " + str(id) + " is not in repository")
        return self.remove_item_at_position(index)

    def updateClient(self, id, new_client):
        clientToUpdate = self.findById(id)

        if clientToUpdate is None:
            raise ClientError("Client having id - " + str(id) + " is not in repository")

        if new_client.getName() != "":
            clientToUpdate.setName(new_client.getName())
        if new_client.getCnp() != "":
            clientToUpdate.setCnp(new_client.getCnp())

    def prepopulate(self):
        client = Client(1, "a", 12421442914)
        self.add(client)
        client = Client(2, "Name", 12421429411)
        self.add(client)

    def findClientByName(self, name):
        """
        Find all movies having the given name
        Input: name - the name of the movie being searched for
        Output: List of movies having the given name.
        """
        result = []
        for currentClient in self.get_all():
            if name == currentClient.getName():
                result.append(currentClient)
        return result