__author__ = 'Vlad'


class Repo:
    def __init__(self):
        self._data = []

    def add(self, newElement):
        self._data.append(newElement)

    def insert_at_position(self, newElement, position):
        self._data.insert(newElement, position)

    def remove_item_at_position(self, position):
        self._data.pop(position)

    def remove_last_item(self):
        self._data.pop()

    def get_all(self):
        return self._data

    def remove_items_between(self, start, end):
        del self._data[start:end]

    def __len__(self):
        return len(self._data)

    def get_item_at_index(self, index):
        return self._data[index]

    '''
    Input: @list The list on which to insert the sequence
        @subsequence the sequence to insert
        @position The position where to add the subsequence
    Output: None
    '''

    def get_subset(self, start, end):
        return self._data[start: end]
