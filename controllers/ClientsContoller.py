from errors import ClientException

__author__ = 'Vlad'


class ClientsContoller:
    def __init__(self, repo):
        self._repo = repo

    def add(self, newElement):
        self._repo.add(newElement)

    def insert_at_position(self, newElement, position):
        self._repo.insert_at_position(newElement, position)

    def remove_item_at_position(self, position):
        self._repo.remove_item_at_position(position)

    def remove_last_item(self):
        self._repo.remove_last_item()

    def get_all(self):
        return self._repo.get_all()

    def addClient(self, movie):
        """
        Add a new movie
        Input: movie - the movie that will be added
        Raises MovieException in case of duplicate movie id.
        """
        self._repo.add(movie)

    def removeClient(self, id):
        """
        Remove the movie with the given id
        Input: id - the id of the movie to remove
        Raises MovieException in case movie having the given id does not exist
        """
        self._repo.remove(id)

    def removeClientByName(self, name):
        for client in self._repo.findClientByName(name):
            self.removeClient(client.getId())

    def getAll(self):
        return self._repo.get_all()

    def isIdUsed(self, id):
        return self._repo.findById(id) is not None

    def updateClient(self, id, new_client):
        self._repo.updateClient(id, new_client)

    def findClientByName(self, name):
        return self._repo.findClientByName(name)

