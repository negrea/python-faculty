__author__ = 'Vlad'

class RentController():

    def __init__(self, rentalRepo, movieRepo, clientRepo):
        self._repo = rentalRepo
        self._movieRepo = movieRepo
        self._clientRepo = clientRepo

    def add(self, newElement):
        self._repo.add(newElement)

    def insert_at_position(self, newElement, position):
        self._repo.insert_at_position(newElement, position)

    def remove_item_at_position(self, position):
        self._repo.remove_item_at_position(position)

    def remove_last_item(self):
        self._repo.remove_last_item()

    def get_all(self):
        return self._repo.get_all()

    def remove(self, rent):
        """
        Remove the rent with the given id
        Input: id - the id of the rent to remove
        Raises RentException in case rent having the given id does not exist
        """
        self._RemoveById(rent.getId())

    def _RemoveById(self, id):
        self._repo.remove(id)

    def getAll(self):
        return self._repo.get_all()

    def findRental(self, client, movie):
        return self._repo.findByNameAndMovie(client, movie)

    # def getMostRentedMovies(self):
    #     allRentals = self.get_all()
    #
    #     moviesCount = []
    #
    #     for rental in allRentals:
    #         if rental.getMovie().getTitle()
    #
    # def _updateMoviesCount(self, moviesCountList, movieTitle):

    def filterRentals(self, client, movie):
        """
        Return a list of rentals performed by the provided client for the provided movie
        client - The client performing the rental. None means all clients
        cars - The rented movie. None means all movies
        """
        result = []
        for rental in self._repo.get_all():
            if client != None and rental.getClient() != client:
                continue
            if movie != None and rental.getMovie() != movie:
                continue
            result.append(rental)
        return result

    def getMostRentedMovies(self):
        """
        Returns an ordered list of the cars in their 'most rented' order, by total number of rental days
        """
        result = []

        '''
        1. Build the data transfer object
        '''
        for movie in self._movieRepo.get_all():
            rentals = self.filterRentals(None, movie)
            result.append(MovieRentals(movie, rentals))

        '''
        2. Sort it
        '''
        result.sort()
        return result

    def getMoviesRentedByClient(self, client):
        return self.filterRentals(client, None)

class MovieRentals:
    def __init__(self, movie, rentals):
        """
        Constructor for this data transfer object
        car - The car for this object
        rentals - The list of rentals where the given car was rented
        """
        self._car = movie
        self._rentals = rentals

    def getCar(self):
        return self._car

    def getRentals(self):
        return self._rentals

    def __lt__(self, carRental):
        """
        < operator required for the sort
        """
        return self.getDaysRented() > carRental.getDaysRented()

    def __str__(self):
        return str(self.getDaysRented()) + " for " + str(self._car)

    def __repr__(self):
        return str(self)

    def getDaysRented(self):
        """
        Returns the number of days the car was rented
        """
        d = 0
        for rental in self._rentals:
            d += len(rental)
        return d